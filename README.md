# Présentation
Ce projet sert à gérer le site vitrine du projet CANULAR.

# Technique
* Le site utilise Hugo et est généré par le CI/CD de gitlab.

# Quelques liens utiles
* Usage de l'intégration continue [ci]
* Site officiel de [hugo]
* [documentation] officielle de Hugo


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[documentation]: https://gohugo.io/overview/introduction/
