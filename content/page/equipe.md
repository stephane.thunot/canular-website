---
title: L'équipe CANULAR
subtitle: 
comments: false
featured_image: "./screenshot_2021-07-07_17-01-46.png"
---



### L'équipe CANULAR

- Christelle Aluome (CATI PROSODIe)
- Christophe Chipeaux (CATI DIISCICO)
- Nicolas Devert (Testeur)
- Catherine Lambrot (Testeur)
- Tovo Rabemanantsoa (CATI PROSODIe)
- **Stéphane Thunot** (CATI GEDEOP)
****
![Equipe](./screenshot_2021-07-07_17-01-46.png "Structure de l'équipe")
<img src="static/images/screenshot_2021-07-07_17-01-46.png" alt="Structure de l'équipe" width="500" height="600">
